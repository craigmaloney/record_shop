all: record_shop.tex record_shop.pdf

record_shop.tex : record_shop.md metadata.yaml
	pandoc -s --template=Pandoc/templates/cs-6x9-pdf.latex -o $@ $^

record_shop.pdf : record_shop.md metadata.yaml
	pandoc -s --template=Pandoc/templates/cs-6x9-pdf.latex --pdf-engine=xelatex -o $@ $^

clean:
	rm *.pdf *.tex
