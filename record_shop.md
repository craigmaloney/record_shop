\tableofcontents{}

\newpage{}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-alteredsnaps-11605716.jpg}
\end{figure}

# Introduction

The record store. A vault of albums banal and rare. The place where you'll find the most popular of recordings sitting next to a recording or artist few people have ever heard of before. 

All waiting to be discovered.

One of my biggest loves is spending time in a record store. I love the discovery of sifting through bins of different recordings locating not only things that I'm actively seeking out but the things that I had no idea existed. I love picking up a random recording, wondering "what the heck is this?", only to discover something new and interesting that I would never have sought out.

This journaling game is about the discovery of a record shop and the connections we have with music. Each card you draw represents a record you might find in a record shop. Think about what the prompt might represent as a record or an artist and a song or album that springs to mind. Don't overthink it! If you can't come up with anything then just riff on what the album might be. Perhaps the recording has an interesting cover or title that matches the prompt. These also don't have to be real albums. Maybe the album is a compilations of unlikely songs, or an imaginary band that released an album. Spend more time writing about the prompts than spending time on Wikipedia looking up whether or not that song or album was recorded by that band.

One area we'd like to break away from conventional record shops is the price of the album. Assume this record shop is willing to part with any album for a fair price. In this game you can afford the album, no matter how expensive the album might be in a conventional record shop. Remember: money is not an obstacle in this record shop.

Above all: there is no one correct way to play this game. If you accidentally attribute an AC/DC song to The Who then that's OK. Perhaps this record shop has a bootleg copy of The Who covering that song that even the most hardcore of fans have yet to hear. This record shop exists completely in your own imagination. If Beethoven making boogie makes you happy then in he's the king of boogie music. Let your mind take you where it wants to go, and let Beethoven roll over to let you play.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-cottonbro-studio-6862354.jpg}
\end{figure}

\newpage{}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-cottonbro-studio-6862357.jpg}
\end{figure}

# How to play

## What you'll need

You'll only need a few things to play this journaling game:

* A standard six sided die (usually referred to as a d6)
* A "standard" deck of 52 playing cards (two jokers preferred, but not required)
    * You may also use the "[Pick-a-Card](https://www.sjgames.com/dice/pickacard/)" dice from Steve Jackson Games, available at [https://www.sjgames.com/dice/pickacard/](https://www.sjgames.com/dice/pickacard/).
* Something to write with (pen, pencil, computer)
* Something to write on (paper, journal, text file)
* (Optional) A timer for the listening room

You may also incorporate other items into your play, such as a music playlist, some of your favorite albums, or what-have-you. Many record shops also have incense so if that helps you get into the "record shop" vibe, feel free to burn some. You might also find things like wearing headphones helps. The idea is to help project yourself into a fictional record shop so whatever helps you enter that head space is perfectly fine to use.

\newpage{}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-yelena-odintsova-11256773.jpg}
\end{figure}


# Playing the game

## Name and describe the store

Write a few sentences to name and describe the store that you're visiting. You may write down a name if you already have one or you may roll a die to select a name at random. Roll the die (d6) up to three times to select a name for the store, starting with the left column and working to the right. Stop when you have a name that works for you or after three rolls, whichever comes first.

| #      | first roll  | second roll | third roll |
|--------|-------------|-------------|------------|
| 1      | Funky       | Vinyl       | Platters   |
| 2      | Tasty       | Disc        | Artisans   |
| 3      | Flat        | Sound       | Circle     |
| 4      | Bespoke     | Record      | Rockers    |
| 5      | Underground | Tune        | Club       |
| 6      | Discount    | Music       | Attic      |

Example: Rather than come up with the name of the shop I decide to roll on the table instead. I roll the die three times and come up with a three (3), a two (2), and a five (5). The name of the record store I'm shopping at is "Flat Disc Club". Satisfied, I write "I arrive at Flat Disc Club" in my journal.

### The store's location

Where is this store located? Sometimes the location of the store can give some hints on what is inside of it. Is it located close to a college and therefore might have more college music, or is it in a strip mall in a small town? The location can be as elaborate as you'd like ("up the top of a mountain", "on a space-station circling Mars", "an alternate universe") or as mundane as you'd like. 

\newpage{}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-alteredsnaps-11994137.jpg}
\end{figure}

\newpage{}

### The store's appearance

Next we'll want to describe the store itself. Write down a sentence to describe this location. You can either use your imagination to describe the location or use a die (d6) to select from the following prompts.

| #      | description                                                                                                                          |
|--------|--------------------------------------------------------------------------------------------------------------------------------------|
| 1      | A brightly colored location with lots of posters of musicians and the faint smell of incense burning from some nondescript location  |
| 2      | A dark, wood-paneled location with vinyl, tapes, and CDs in shelves and a bearded guy at the counter watching your every move        |
| 3      | A flea-market like atmosphere with rows of folding tables and boxes of records, tapes, and CDs scattered throughout                  |
| 4      | A meticulous, professional store with sales staff busily stocking shelves and a person at the counter ringing up customers           |
| 5      | A hodgepodge of various collectibles surround a few stacks of records and CDs hidden in a corner                                     |
| 6      | A garage / boot sale with a table holding some records and CDs                                                                       |

### The store's music

Most record shops will have some form of music playing, either for the enjoyment of the staff or the patrons. Write a sentence or more about the music that the store is playing. You can either imagine the music or roll a die (d6) from the table below.

| #      | description                                                       |
|--------|-------------------------------------------------------------------|
| 1      | A soulful tune by an artist you've never heard of before          |
| 2      | A twangy, steel guitar-filled country-song ballad                 |
| 3      | A thumping Detroit Techno mix                                     |
| 4      | Classical music from an unfamiliar composer                       |
| 5      | A classic rock album that is both familiar to you and somehow new |
| 6      | "It's music, captain, but not as we know it"                      |

## Number of albums

Decide how many significant albums you'll discover in this particular record shop. You can decide this number up-front or you can draw one card and use that to determine the number of albums you'll discover (Jacks, Queens, Kings, Jokers count as 10; Aces count as 11). 

Example: I sit down to journal about my record shop visit. I draw an 8 of Diamonds, which means there will be 8 significant recordings for me to journal about. Obviously I can journal about less if I want but the maximum number of significant recordings I'll find on this trip is 8.

## Browsing the stacks

The heart of the game is in actually finding songs or albums on the various recordings in the shop and writing about them. Write as little or as much as you'd like about the recordings you discover.

### Selecting a recording

Each card represents a recording. Draw one card and look at the **[Recordings](#recordings)** table below for what recording you find in the stacks of recordings in the shop. Think of a song or album that fits this recording. Take as much time as you'd like but don't overthink it. If you can't come up with a song or an album that fits the card then put that card aside for the time being and select another card. If you want you can revisit that card later to learn what album it represents. If you still can't think of a song or album that fits the card then you may discard the card and continue browsing the stacks.

Once you have selected the record write down the artist and the album or song title. 

Here are some prompts you may use. Feel free to use as many of these as you wish or invent your own:

* What format is the recording? (CD, Vinyl LP, 45 RPM, 8 Track, Edison Cylinder, etc.)
* What songs are on the recording?
* What do those songs mean to you?
* Who is the artist and what significance do they have to you?
* When did you first hear this recording?
* How did you hear about this recording?
* Why do you like / dislike this recording?
* Who do you know who also likes / dislikes this recording?
* What is the significance of this recording to the artist(s) involved?
* What anecdotes can you recall about this recording?
* Which song(s) on the recording do you enjoy the most? The least?
* What feelings do you have about this recording?
* Why is this recording in this shop?

You may then decide if you're going to buy the recording or put it back. Write a bit about your reasons for wanting to keep the recording or leave it there. Remember, price is not an obstacle in this record shop.

\newpage{}

## The Listening Room (Optional)

If you want the record shop can have a "listening room" where you can put on some headphones and listen to the song or album you've selected. Feel free to search for the song or album online and listen to it. You can use this time to write down your feelings about the song and anything that comes to mind (How well the recording sounds, how much you enjoy this song, etc.). You may also look on Wikipedia or other search engines to find out any additional details about this recording, but please exercise caution here. Sometimes we can fall into rabbit-holes when it comes to music. If you want you can set a timer prior to doing your search. (10 minutes is probably enough time). When the timer expires you can journal how the shop keeper asks you to let other customers listen to their records, or any other encounter you can think of. Keep it light. Nobody gets kicked out of this shop for loitering.

## Recordings

**Joker**: A song or album that you had no idea existed up until now

Allow yourself to go wild with this. This can be a collaboration with artists that you weren't aware of, a rare album from a band you like, or something so completely "out there" that it's outside of your own experience.

\newpage{}

### Spades
| #          | Description                                                                                   |
|------------|-----------------------------------------------------------------------------------------------|
| Ace        | A song or album that must be played loud in order to appreciate it                            |
| King       | A song or album from a performer that is regarded as a classic                                |
| Queen      | A song or album with a strong lead performer                                                  |
| Jack       | A song or album that is random (a compilation, or an obscure album)                           |
| Ten        | A song or album that is perfect in every way                                                  |
| Nine       | A song or album that never fails to pick you up                                               |
| Eight      | A song or album where one listen is enough                                                    |
| Seven      | A song or album that was overplayed                                                           |
| Six        | A song or album that reminds you of something sad                                             |
| Five       | A song or album in a genre that you haven't heard before                                      |
| Four       | A song or album featuring a performer you like with other performers you're unfamiliar with   |
| Three      | A song or album that you find relaxing                                                        |
| Two        | A song with themes of intimacy or relating to being intimate                                  |

\newpage{}

### Hearts
| #          | Description                                                                                   |
|------------|-----------------------------------------------------------------------------------------------|
| Ace        | A song or album you can't help but love                                                       |
| King       | A song or album from a performer that is regarded as the pinnacle of their genre              |
| Queen      | A song or album that has a theme about love                                                   |
| Jack       | A song or album that hides a darker theme                                                     |
| Ten        | A song or album that someone recommended to you as perfect                                    |
| Nine       | A song or album that is satisfying to you                                                     |
| Eight      | A song or album that is pleasant to listen to                                                 |
| Seven      | A song or album that you once loved but now can't stand                                       |
| Six        | A song or album from a performer known for their charity                                      |
| Five       | A song or album that makes you jealous                                                        |
| Four       | A song or album that took you a long time to enjoy                                            |
| Three      | A song or album from a performer you no longer enjoy                                          |
| Two        | A song or album that you feel didn't get the attention it deserved                            |

\newpage{}

### Clubs
| #          | Description                                                                                   |
|------------|-----------------------------------------------------------------------------------------------|
| Ace        | A song or album that has an incredible bass line                                              |
| King       | A song or album from a performer that can really wind up a crowd                              |
| Queen      | A song or album from a performer that has remarkable clothes                                  |
| Jack       | A song or album from an artist that you've followed for a long time                           |
| Ten        | A song or album that is a "Desert Island Disc"                                                |
| Nine       | A song or album that you weren't allowed to have as a child                                   |
| Eight      | A song or album that is one of your friend's favorites                                        |
| Seven      | A song or album you remember while flirting with someone                                      |
| Six        | A song or album from a successful artist                                                      |
| Five       | A song or album from an artist that an older relative enjoyed                                 |
| Four       | A song or album that is all over the place                                                    |
| Three      | A song or album that defies explanation                                                       |
| Two        | A song or album that was a huge disappointment                                                |

\newpage{}

### Diamonds
| #          | Description                                                                                   |
|------------|-----------------------------------------------------------------------------------------------|
| Ace        | A song or album that you'd use to end a fight with a neighbor                                 |
| King       | A song or album that is considered evil                                                       |
| Queen      | A song or album with a snake on the cover                                                     |
| Jack       | A song or album that is considered a classic but leaves you cold                              |
| Ten        | A song or album about money                                                                   |
| Nine       | A song or album with a solid color for the cover                                              |
| Eight      | A song or album about moving on                                                               |
| Seven      | A song or album with amazing artwork                                                          |
| Six        | A song or album with good songs but terrible music videos                                     |
| Five       | A song or album from an performer who met a tragic end                                        |
| Four       | A song or album from a band that has few original members                                     |
| Three      | A song or album from a band in turmoil                                                        |
| Two        | A song or album that caused a controversy when it was released                                |

# Checking Out

When you have selected your albums it's time to check out of the store. Decide which albums you are taking home with you and take them to the checkout counter.

Imagine the interaction between you and the person behind the counter. What do they look like? What clothes are they wearing? What is their demeanor?

Write down any interactions or comments that this person might have with you. Perhaps you both were at the same concert for this or a related band, or perhaps this person is a long-time fan of this artist. Maybe they're struck by the cover and want to know more about the album and anything you can tell them. Perhaps they're just making some smalltalk while ringing up your purchase.

\newpage{}

If you're having trouble imagining this person feel free to roll a d6 on the following table:

| #          |  Description                                                                                  |
|------------|-----------------------------------------------------------------------------------------------|
| 1          | A hyperactive music fan who wants to know more about what you found in their store            |
| 2          | A somewhat incoherent hippie busily scribbling down what you bought onto a piece of paper     |
| 3          | A well-dressed individual who comments on your excellent taste in music                       |
| 4          | A young goth with lots of pins on their vest and a quiet demeanor                             |
| 5          | The owner of the shop who asks questions about how you found their shop                       |
| 6          | A kind individual who is curious about each recording you've selected                         |

One thing this person won't do is belittle your choice of albums. Even if your choice of music isn't their thing they'll always be respectful and polite about it.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-alteredsnaps-11994107.jpg}
\end{figure}

\newpage{}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-cottonbro-studio-6862352.jpg}
\end{figure}

# Overall Theme

Think about the overall theme of your trip. What did each of these recordings and artists represent? It doesn't have to be profound. Sometimes the theme is "hodge-podge of things I like. That's OK. But if you sense there's an overarching theme to this trip write a few sentences about it and what this trip meant to you.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-cottonbro-studio-3693108.jpg}
\end{figure}

# Listening at Home

If you wish you can write about yourself going over these albums and giving them a listen at home. Write about how you listen to these albums or any experiences you share with others. If you want you can take this opportunity to listen to any actual albums you found along the way. Maybe you can write about when you hope to go back to this shop to find more.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/pexels-alteredsnaps-11994111.jpg}
\end{figure}


# Final Thoughts and Designer Notes

I hope you enjoyed this experience. One of my favorite things is to go to record shops and find new music. Sometimes I've found things that I had no idea existed. I found the work of Isao Tomita at a used record shop. It was his version of Holst's "The Planets". When I put the disc in to the car CD player I had no idea what I'd gotten myself into. Later I did more exploration and found more of his music and became an instant admirer and fan. That's just one example of the wonders I've found at record shops. I've become friends and acquaintances with many record shop owners over the years. My hope with this game is that you can replicate that same feeling of joy and wonder in finding a new record shop and exploring its treasures.

If you have any questions about the game or would like to share feedback please contact me at [https://decafbad.net/contact](https://decafbad.net).
