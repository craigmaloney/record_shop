# Record Shop

Record Shop Game

("File:Rough Trade NYC, record store in New York City (2014-11-28 by David Hilowitz).jpg" by [David Hilowitz](https://commons.wikimedia.org/wiki/File:Rough_Trade_NYC,_record_store_in_New_York_City_(2014-11-28_by_David_Hilowitz).jpg) licensed under a [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/deed.en). Changes: cropped, resized.)
